/* (c) https://github.com/MontiCore/monticore */
package tuning;

import functionblocks.PID.PID;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import simulation.vehicle.Vehicle;
import simulation.vehicle.VehicleActuator;
import simulation.simulator.Simulator;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by s.erlbeck on 06.03.2017.
 */
public class TestVelocityTuning extends TestCase {

    /**
     * Simulation stuff
     */
    private SimulationWrapper wrap;
    private Simulator sim;
    private Vehicle vehicle;
    private VehicleActuator motor;
    private VehicleActuator brakes;

    /**
     * Tuning stuff
     */
    //delta time in milliseconds
    private final int deltaTime = 10;
    //sample rate of 2 means: 2 sim-calls for 1 controller-call
    private final int sampleRate = 1;
    //delta time of the PID in seconds!!!
    private final double deltaPID = sampleRate * deltaTime / 1000.0;
    //the number of measured values
    private final int numberOfSamples = 5000;

    //the precision of the analysis of the function
    private final double eps = 0.1;

    //controller
    private PID velocityPID;

    //the arrays to save the measured values
    private double[] measuredSpeed;
    //the results
    private double velocity_k_krit;
    private double velocity_t_krit;



    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public TestVelocityTuning(String testName) {
        super(testName);
        wrap = new SimulationWrapper();
        sim = wrap.getSim();
        vehicle = wrap.getVehicle();
        motor = wrap.getMotor();
        brakes = wrap.getBrakes();

    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(TestVelocityTuning.class);
    }

    /**
     * this code was used to tune the controllers (should still work locally)
     * however, since it may use code, which was changed to deprecated, it may result in a Jenkins crash
     */
    /*
    public void testTuneZiNi() {

        measuredSpeed = new double[numberOfSamples];

        List<Integer> maxima = new LinkedList<Integer>();
        List<Integer> minima = new LinkedList<Integer>();

        double k_p = eps;
        int status = 0;
        int maxIter = 100;
        int i = 0;
        double lowerBound = 0;
        double upperBound = -1;
        boolean foundUpperBound = false;

        do {

            //System.out.println(k_p);

            velocityPID = new PID(k_p, 0, 0);
            runSimulationVelocity();
            extractExtrema(measuredSpeed, maxima, minima);
            status = analyseExtrema(measuredSpeed, maxima, minima);



            //no upper bound found yet, so we use linear search
            if(!foundUpperBound) {
                if(status < 2) {
                    lowerBound = k_p;
                    k_p++;
                } else if(status == 3){
                    //now we found the upper bound
                    //initialise binary search
                    foundUpperBound = true;
                    upperBound = k_p;
                    k_p = (upperBound - lowerBound) / 2.0;
                }
            } else if(foundUpperBound) {
                if(status < 2) {
                    lowerBound = k_p;
                } else if(status == 3) {
                    upperBound = k_p;
                }
                k_p = (upperBound - lowerBound) / 2.0;
            }

            i++;

            //System.out.println(k_p);
        } while(status != 2 && i < maxIter);

        printResult("Velocity automatically", measuredSpeed, maxima, minima);


        if(i == maxIter) {
            System.out.println("Convergence issues. The optimal k_p could not be found");
            assertTrue(true);
        } else {

            velocity_k_krit = k_p;

            //assume that the period stays roughly the same
            velocity_t_krit = (maxima.get(1) - maxima.get(0)) * deltaTime;

            System.out.println("k_krit for Velocity: " + velocity_k_krit);
            System.out.println("T_krit for Velocity: " + velocity_t_krit);


            assertTrue(true);
        }
    }
    */


    //rename to testtuneManually to run the tuning
    public void tuneManually() {
        measuredSpeed = new double[numberOfSamples];

        List<Integer> maxima = new LinkedList<Integer>();
        List<Integer> minima = new LinkedList<Integer>();


         // k_krit is about 3
         // t_krit is about 2 seconds

        velocityPID = new PID(3, 0, 0);
        runSimulationVelocity();
        extractExtrema(measuredSpeed, maxima, minima);
        analyseExtrema(measuredSpeed, maxima, minima);

        printResult("Velocity manually", measuredSpeed, maxima, minima);

        assertTrue(true);
    }



    /**
     * a mock test so there is a test in this class
     */
    public void test() {
        assertTrue(true );
    }


    public void runSimulationVelocity() {
/*
        //init controller stuff
        Map<String, Object> input = new HashMap<>(2);
        double outputVel = 0;
        double outputAcc = 0;
        Sensor sensor = vehicle.getSensorByType(BusEntry.SENSOR_VELOCITY).get();


        input.put(ConnectionEntry.PID_desired_value.toString(), 5D);
        input.put(ConnectionEntry.PID_delta_time.toString(), deltaPID);



        //Start simulation
        sim.stopAfter(deltaTime);
        sim.startSimulation();

        //i is in milliseconds
        for(int i = deltaTime; i <= numberOfSamples*deltaTime; i += deltaTime) {

            sensor.update();

            measuredSpeed[i / deltaTime - 1] = (Double)sensor.getValue();


            //dont call the PID in every simulation step
            if(i % (sampleRate*deltaTime) == 0) {
                //controller
                input.put(ConnectionEntry.PID_current_value.toString(), sensor.getValue());
                velocityPID.setInputs(input);
                velocityPID.execute();
                outputVel = (Double) velocityPID.getOutputs().get(ConnectionEntry.PID_new_controlled_value.toString());
                outputAcc = outputVel / deltaPID;

                try {
                    if (outputAcc < 0) {
                        //brake
                        outputAcc *= -1;
                        if (outputAcc > vehicle.VEHICLE_DEFAULT_BRAKES_ACCELERATION_MAX) {
                            outputAcc = vehicle.VEHICLE_DEFAULT_BRAKES_ACCELERATION_MAX;
                        }
                        motor.setActuatorValueTarget(0);
                        brakes.setActuatorValueTarget(outputAcc);
                    } else {
                        //accelerate
                        if (outputAcc > vehicle.VEHICLE_DEFAULT_MOTOR_ACCELERATION_MAX) {
                            outputAcc = vehicle.VEHICLE_DEFAULT_MOTOR_ACCELERATION_MAX;
                        }
                        motor.setActuatorValueTarget(outputAcc);
                        brakes.setActuatorValueTarget(0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            sim.extendSimulationTime(deltaTime);
            sim.startSimulation();
        }


        sim.extendSimulationTime(10);
        sim.setPausedInFuture(false);
        sim.startSimulation();


        vehicle.getStatusLogger().logStatusMemory();
 */
    }


    /**
     * HELPER
     */


    /**
     * prints the results in a file
     * @param file the resulting filename will be "ZiNi + file + .txt"
     * @param values the values to print
     * @param maxima the list of maxima
     * @param minima the list of minima
     */
    private void printResult(String file, double values[], List<Integer> maxima, List<Integer> minima) {

        try{
            PrintWriter writer = new PrintWriter(file + ".txt");

            //iterator init
            Iterator<Integer> maxIterator = maxima.iterator();
            Iterator<Integer> minIterator = minima.iterator();
            int maxIndex = -1;
            int minIndex = -1;
            if(maxIterator.hasNext()) {
                maxIndex = maxIterator.next();
            }
            if(minIterator.hasNext()) {
                minIndex = minIterator.next();
            }

            for(int i = 0; i < values.length; i++) {
                if(maxIndex == i) {
                    writer.println(values[i] + "\t(max)");
                    if(maxIterator.hasNext()) {
                        maxIndex = maxIterator.next();
                    }
                } else if(minIndex == i) {
                    writer.println(values[i] + "\t(min)");
                    if(minIterator.hasNext()) {
                        minIndex = minIterator.next();
                    }
                } else {
                    writer.println(values[i]);
                }
            }

            writer.close();
        } catch (IOException e) {
            // do something, e.g. cry out for help
            System.out.println("Problem with File.");
        }
    }


    /**
     * calculates all local extrema in the array values, assuming the resolution is high enough
     * the lists will be cleared in the beginning
     * @param values the array which extrema we want to examine
     * @param maxima the list, in which the indices of maxima will be stored
     * @param minima the list, in which the indices of minima will be stored
     */
    private void extractExtrema(double[] values, List<Integer> maxima, List<Integer> minima) {

        //clear the lists
        maxima.clear();
        minima.clear();

        /*
        Yeah, this code is probably much longer than needed, but values can be pretty long
        and this code is probably a bit faster than a shortened version
        */

        boolean oldRising = values[1] > values[0];
        boolean oldFalling = values[1] < values[0];
        boolean newRising;
        boolean newFalling;

        for(int i = 1; i < values.length - 1; i++) {
            newRising = values[i + 1] > values[i];
            newFalling = values[i + 1] < values[i];
            if(oldRising) {
                if(newFalling) {
                    //here the values were rising and are now falling, thus we found a local maximum
                    maxima.add(i);
                }
            } else if(oldFalling) {
                if(newRising) {
                    //here the values were falling and are now rising, thus we found a local minimum
                    minima.add(i);
                }
            }
            //as we will consider the following value we can save the current slope behaviour
            oldRising = newRising;
            oldFalling = newFalling;
        }
    }

    /**
     * analyses the function in values with the help of the provided maxima and minima
     * @param values the function
     * @param maxima the list of maxima
     * @param minima the list of minima
     * @return
     *          0 iff function is not oscillating at all
     *          1 iff function oscillates but the oscillation decays
     *          2 iff function oscillates in a stable manner
     *          3 iff function starts to diverge due to oscillation
     */
    private int analyseExtrema(double[] values, List<Integer> maxima, List<Integer> minima) {

        if(maxima.isEmpty() || minima.isEmpty()) {
            //function is not oscillating
            return 0;
        }

        //1. analyse Maxima
        double meanOfMax = 0;
        int lastIndexMax = maxima.get(0);

        //calculate arithmetic mean
        for(int i : maxima) {
            meanOfMax += values[i];

            if(values[lastIndexMax] < values[i]) {
                //oscillation gets too strong over time
                return 3;
            }
            lastIndexMax = i;
        }
        meanOfMax /= maxima.size();

        //check whether all maxima are in an eps neighbourhood of the mean
        for(int i : maxima) {
            if(!(meanOfMax - eps < values[i] && values[i] < meanOfMax + eps)) {
                //oscillation not stable enough (too much decay)
                return 1;
            }
        }


        //1. analyse Minima
        double meanOfMin = 0;
        int lastIndexMin = minima.get(0);

        //calculate arithmetic mean
        for(int i : minima) {
            meanOfMin += values[i];

            if(values[lastIndexMin] > values[i]) {
                //oscillation gets too strong over time
                return 3;
            }
            lastIndexMin = i;
        }
        meanOfMin /= minima.size();

        //check whether all minima are in an eps neighbourhood of the mean
        for(int i : minima) {
            if(!(meanOfMin - eps < values[i] && values[i] < meanOfMin + eps)) {
                //oscillation not stable enough (too much decay)
                return 1;
            }
        }


        //everything seems to be fine then
        return 2;
    }

}

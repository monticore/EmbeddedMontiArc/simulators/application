/* (c) https://github.com/MontiCore/monticore */
package tuning;

import simulation.environment.osm.ParserSettings;
import simulation.environment.weather.WeatherSettings;
import simulation.simulator.SimulationPlotter2D;
import simulation.simulator.Simulator;
import simulation.vehicle.*;

/**
 * Simple wrapper class which sets up the simulation in its constructor
 * does not work because deprecated code causes the remote repository to crash
 * Created by s.erlbeck on 15.03.2017.
 */
public class SimulationWrapper {

    private Simulator sim;
    private WeatherSettings weatherSettings;
    private ParserSettings parserSettings;
    private PhysicalVehicleBuilder physicalVehicleBuilder;
    private PhysicalVehicle physicalVehicle;
    private Vehicle vehicle;
    private VehicleActuator motor;
    private VehicleActuator steering;
    private VehicleActuator brakes;
    private SimulationPlotter2D plotter2D;

    public SimulationWrapper() {
        this.setUpSimulation();
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public VehicleActuator getMotor() {
        return motor;
    }

    public Simulator getSim() {
        return sim;
    }

    public VehicleActuator getSteering() {
        return steering;
    }

    public VehicleActuator getBrakes() {
        return brakes;
    }

    /**
     * this code was used to tune the controllers (should still work locally)
     * however, since it uses code, which was changed to deprecated, it results in a Jenkins crash
     */
     
    private void setUpSimulation() {
/*
        //Set update frequency to 100 loop iterations per second
        sim = Simulator.getSharedInstance();
        sim.setSimulationLoopFrequency(100);
        sim.setSimulationType(SimulationType.SIMULATION_TYPE_FIXED_TIME);

        // Synchronous simulation
        sim.setSynchronousSimulation(true);
        sim.setPausedInFuture(true);

        // Flexibility in Z coordinates
        weatherSettings = new WeatherSettings(Weather.SUNSHINE);
        parserSettings = new ParserSettings("/map_ahornstrasse.osm", ParserSettings.ZCoordinates.ALLZERO);

        try {
            WorldModel.init(parserSettings, weatherSettings);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Create a new vehicle
        physicalVehicleBuilder = PhysicalVehicleBuilder.getInstance();
        physicalVehicle = physicalVehicleBuilder.buildPhysicalVehicle(Optional.empty(), Optional.empty(), Optional.empty());
        SensorUtil.sensorAdder(physicalVehicle);
        vehicle = physicalVehicle.getSimulationVehicle();

        //Create random faults for vehicle's internal fault memory
        vehicle.setStatusLogger(new RandomStatusLogger());

        // Set actuator values for testing
        motor = vehicle.getVehicleActuator(VehicleActuatorType.VEHICLE_ACTUATOR_TYPE_MOTOR);
        steering = vehicle.getVehicleActuator(VehicleActuatorType.VEHICLE_ACTUATOR_TYPE_STEERING);
        brakes = vehicle.getVehicleActuator(VehicleActuatorType.VEHICLE_ACTUATOR_TYPE_BRAKES);

        // Set some initial actuator values
        try {
            motor.setActuatorValueTarget(0);
            steering.setActuatorValueTarget(0);
            brakes.setActuatorValueTarget(0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Create a plotter object
        plotter2D = new SimulationPlotter2D();
        sim.registerLoopObserver(plotter2D);

        // Add physicalVehicle to simulation
        // Position: Bus stop Ahornstrasse in front of Informatikzentrum
        sim.registerAndPutObject(physicalVehicle, 1584.31626412008, 877.404690000371, 0.0);
*/
    }
    
}

/* (c) https://github.com/MontiCore/monticore */
package tuning;

import TestExampleGenerators.PathGenerator;
import functionblocks.PID.PID;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.math3.linear.ArrayRealVector;
import simulation.vehicle.Vehicle;
import simulation.vehicle.VehicleActuator;
import simulation.simulator.Simulator;
import commons.controller.commons.Vertex;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by s.erlbeck on 06.03.2017.
 */
public class TestSteeringTuning extends TestCase {

    /**
     * Simulation stuff
     */
    private SimulationWrapper wrap;
    private Simulator sim;
    private Vehicle vehicle;
    private VehicleActuator motor;
    private VehicleActuator brakes;
    private VehicleActuator steering;

    /**
     * Tuning stuff
     */
    //delta time in milliseconds
    private final int deltaTime = 10;
    //sample rate of 2 means: 2 sim-calls for 1 controller-call
    private final int sampleRate = 1;
    //delta time of the PID in seconds!!!
    private final double deltaPID = sampleRate * deltaTime / 1000.0;
    //the number of measured values
    private final int numberOfSamples = 2000;

    //the precision of the analysis of the function
    private final double eps = 0.1;

    //controller
    private PID velocityPID;
    private PID steeringPID;

    //the arrays to save the measured values
    private double[] measuredSteering;
    //the results
    private double velocity_k_krit;
    private double velocity_t_krit;



    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public TestSteeringTuning(String testName) {
        super(testName);
        wrap = new SimulationWrapper();
        sim = wrap.getSim();
        vehicle = wrap.getVehicle();
        motor = wrap.getMotor();
        brakes = wrap.getBrakes();
        steering = wrap.getSteering();
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(TestSteeringTuning.class);
    }



    //rename to testtuneManually to run the tuning
    public void tuneManually() {
        measuredSteering = new double[numberOfSamples];

        List<Integer> maxima = new LinkedList<Integer>();
        List<Integer> minima = new LinkedList<Integer>();


        velocityPID = new PID(0.6, 0.6, 0.4);
        steeringPID = new PID(0.2, 0.0, 0.5);
        runSimulationSteering();
        extractExtrema(measuredSteering, maxima, minima);
        analyseExtrema(measuredSteering, maxima, minima);

        printResult("Steering manually", measuredSteering, maxima, minima);

        assertTrue(true);
    }



    private List<Vertex> createPath() {
        PathGenerator gen = new PathGenerator();

        return gen.generateStraightLine(5000, 1584.31626412008, 880, new ArrayRealVector(new double[]{0.0, 1.0, 0.0}), 0);
    }

    public void runSimulationSteering() {
/*
        //init controller stuff
        Map<String, Object> inputVel = new HashMap<>(2);
        Map<String, Object> inputSteering = new HashMap<>(2);
        double outputVel = 0;
        double outputAcc = 0;
        double outputSteering = 0;
        Sensor sensorVel = vehicle.getSensorByType(BusEntry.SENSOR_VELOCITY).get();
        Sensor steeringSensor = vehicle.getSensorByType(BusEntry.SENSOR_STEERING).get();
        Sensor compass = vehicle.getSensorByType(BusEntry.SENSOR_COMPASS).get();
        Sensor gps = vehicle.getSensorByType(BusEntry.SENSOR_GPS_COORDINATES).get();


        inputVel.put(ConnectionEntry.PID_desired_value.toString(), 1.0);
        inputVel.put(ConnectionEntry.PID_delta_time.toString(), deltaPID);
        inputVel.put(ConnectionEntry.PID_current_value.toString(), 0);

        inputSteering.put(ConnectionEntry.PID_desired_value.toString(), 0.0);
        inputSteering.put(ConnectionEntry.PID_delta_time.toString(), deltaPID);
        inputSteering.put(ConnectionEntry.PID_current_value.toString(), 0);

        //Steering stuff
        FunctionBlock steeringLogic = new SteeringLogic();
        Map<String, Object> inputLogic = new HashMap<>(7);

        //not needed
        inputLogic.put(ConnectionEntry.STEERING_LOGIC_distance_to_left.toString(), 0.0);
        inputLogic.put(ConnectionEntry.STEERING_LOGIC_distance_to_right.toString(), 0.0);
        inputLogic.put(ConnectionEntry.STEERING_LOGIC_vanishing_angle.toString(), 0.0);
        //only set once
        inputLogic.put(ConnectionEntry.STEERING_LOGIC_path.toString(), this.createPath());
        //sensors
        inputLogic.put(ConnectionEntry.STEERING_LOGIC_global_orientation.toString(), 0.0);
        inputLogic.put(ConnectionEntry.STEERING_LOGIC_current_velocity.toString(), 0.0);
        inputLogic.put(ConnectionEntry.STEERING_LOGIC_gps_coordinates.toString(), 0.0);


        try {
            motor.setActuatorValueTarget(1.0);
            brakes.setActuatorValueTarget(0.0);
            steering.setActuatorValueTarget(vehicle.VEHICLE_DEFAULT_STEERING_ANGLE_MAX);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Start simulation
        sim.stopAfter(100 * deltaTime);
        sim.startSimulation();

        //i is in milliseconds
        for(int i = deltaTime; i <= numberOfSamples*deltaTime; i += deltaTime) {

            sensorVel.update();
            steeringSensor.update();
            compass.update();
            gps.update();

            measuredSteering[i / deltaTime - 1] = (Double)steeringSensor.getValue();


            //don't call the PID in every simulation step
            if(i % (sampleRate*deltaTime) == 0) {
                //input vel pid and logic
                inputVel.put(ConnectionEntry.PID_current_value.toString(), sensorVel.getValue());
                inputLogic.put(ConnectionEntry.STEERING_LOGIC_current_velocity.toString(), sensorVel.getValue());
                inputLogic.put(ConnectionEntry.STEERING_LOGIC_global_orientation.toString(), compass.getValue());
                inputLogic.put(ConnectionEntry.STEERING_LOGIC_gps_coordinates.toString(), gps.getValue());

                //execute logic
                steeringLogic.setInputs(inputLogic);
                steeringLogic.execute();
                //input steering pid
                inputSteering.put(ConnectionEntry.PID_desired_value.toString(),
                        steeringLogic.getOutputs().get(ConnectionEntry.STEERING_LOGIC_steering_angle.toString()));
                inputSteering.put(ConnectionEntry.PID_current_value.toString(), steeringSensor.getValue());

                //execute velocity
                velocityPID.setInputs(inputVel);
                velocityPID.execute();
                outputVel = (Double) velocityPID.getOutputs().get(ConnectionEntry.PID_new_controlled_value.toString());
                outputAcc = outputVel / deltaPID;

                //execute steering PID
                steeringPID.setInputs(inputSteering);
                steeringPID.execute();
                outputSteering = (Double) steeringPID.getOutputs().get(ConnectionEntry.PID_new_controlled_value.toString());
                outputSteering += (Double) steeringSensor.getValue();

                try {
                    if (outputAcc < 0) {
                        //brake
                        outputAcc *= -1;
                        if (outputAcc > vehicle.VEHICLE_DEFAULT_BRAKES_ACCELERATION_MAX) {
                            outputAcc = vehicle.VEHICLE_DEFAULT_BRAKES_ACCELERATION_MAX;
                        }
                        motor.setActuatorValueTarget(0);
                        brakes.setActuatorValueTarget(outputAcc);

                        //just a line doing nothing...
                        int p = (int)20.0 / 2;

                    } else {
                        //accelerate
                        if (outputAcc > vehicle.VEHICLE_DEFAULT_MOTOR_ACCELERATION_MAX) {
                            outputAcc = vehicle.VEHICLE_DEFAULT_MOTOR_ACCELERATION_MAX;
                        }
                        motor.setActuatorValueTarget(outputAcc);
                        brakes.setActuatorValueTarget(0);

                    }
                    if(outputSteering > vehicle.VEHICLE_DEFAULT_STEERING_ANGLE_MAX) {
                        outputSteering = vehicle.VEHICLE_DEFAULT_STEERING_ANGLE_MAX;
                    }
                    if(outputSteering < vehicle.VEHICLE_DEFAULT_STEERING_ANGLE_MIN) {
                        outputSteering = vehicle.VEHICLE_DEFAULT_STEERING_ANGLE_MIN;
                    }
                    steering.setActuatorValueTarget(outputSteering);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            sim.extendSimulationTime(deltaTime);
            sim.startSimulation();
        }


        sim.extendSimulationTime(10);
        sim.setPausedInFuture(false);
        sim.startSimulation();


        vehicle.getStatusLogger().logStatusMemory();
*/
    }
    
    /**
     * a mock test so there is a test in this class
     */
    public void test() {
        assertTrue(true );
    }


    /**
     * HELPER
     */


    /**
     * prints the results in a file
     * @param file the resulting filename will be "ZiNi + file + .txt"
     * @param values the values to print
     * @param maxima the list of maxima
     * @param minima the list of minima
     */
    private void printResult(String file, double values[], List<Integer> maxima, List<Integer> minima) {

        try{
            PrintWriter writer = new PrintWriter(file + ".txt");

            //iterator init
            Iterator<Integer> maxIterator = maxima.iterator();
            Iterator<Integer> minIterator = minima.iterator();
            int maxIndex = -1;
            int minIndex = -1;
            if(maxIterator.hasNext()) {
                maxIndex = maxIterator.next();
            }
            if(minIterator.hasNext()) {
                minIndex = minIterator.next();
            }

            for(int i = 0; i < values.length; i++) {
                if(maxIndex == i) {
                    writer.println(values[i] + "\t(max)");
                    if(maxIterator.hasNext()) {
                        maxIndex = maxIterator.next();
                    }
                } else if(minIndex == i) {
                    writer.println(values[i] + "\t(min)");
                    if(minIterator.hasNext()) {
                        minIndex = minIterator.next();
                    }
                } else {
                    writer.println(values[i]);
                }
            }

            writer.close();
        } catch (IOException e) {
            // do something, e.g. cry out for help
            System.out.println("Problem with File.");
        }
    }


    /**
     * calculates all local extrema in the array values, assuming the resolution is high enough
     * the lists will be cleared in the beginning
     * @param values the array which extrema we want to examine
     * @param maxima the list, in which the indices of maxima will be stored
     * @param minima the list, in which the indices of minima will be stored
     */
    private void extractExtrema(double[] values, List<Integer> maxima, List<Integer> minima) {

        //clear the lists
        maxima.clear();
        minima.clear();

        /*
        Yeah, this code is probably much longer than needed, but values can be pretty long
        and this code is probably a bit faster than a shortened version
        */

        boolean oldRising = values[1] > values[0];
        boolean oldFalling = values[1] < values[0];
        boolean newRising;
        boolean newFalling;

        for(int i = 1; i < values.length - 1; i++) {
            newRising = values[i + 1] > values[i];
            newFalling = values[i + 1] < values[i];
            if(oldRising) {
                if(newFalling) {
                    //here the values were rising and are now falling, thus we found a local maximum
                    maxima.add(i);
                }
            } else if(oldFalling) {
                if(newRising) {
                    //here the values were falling and are now rising, thus we found a local minimum
                    minima.add(i);
                }
            }
            //as we will consider the following value we can save the current slope behaviour
            oldRising = newRising;
            oldFalling = newFalling;
        }
    }

    /**
     * analyses the function in values with the help of the provided maxima and minima
     * @param values the function
     * @param maxima the list of maxima
     * @param minima the list of minima
     * @return
     *          0 iff function is not oscillating at all
     *          1 iff function oscillates but the oscillation decays
     *          2 iff function oscillates in a stable manner
     *          3 iff function starts to diverge due to oscillation
     */
    private int analyseExtrema(double[] values, List<Integer> maxima, List<Integer> minima) {

        if(maxima.isEmpty() || minima.isEmpty()) {
            //function is not oscillating
            return 0;
        }

        //1. analyse Maxima
        double meanOfMax = 0;
        int lastIndexMax = maxima.get(0);

        //calculate arithmetic mean
        for(int i : maxima) {
            meanOfMax += values[i];

            if(values[lastIndexMax] < values[i]) {
                //oscillation gets too strong over time
                return 3;
            }
            lastIndexMax = i;
        }
        meanOfMax /= maxima.size();

        //check whether all maxima are in an eps neighbourhood of the mean
        for(int i : maxima) {
            if(!(meanOfMax - eps < values[i] && values[i] < meanOfMax + eps)) {
                //oscillation not stable enough (too much decay)
                return 1;
            }
        }


        //1. analyse Minima
        double meanOfMin = 0;
        int lastIndexMin = minima.get(0);

        //calculate arithmetic mean
        for(int i : minima) {
            meanOfMin += values[i];

            if(values[lastIndexMin] > values[i]) {
                //oscillation gets too strong over time
                return 3;
            }
            lastIndexMin = i;
        }
        meanOfMin /= minima.size();

        //check whether all minima are in an eps neighbourhood of the mean
        for(int i : minima) {
            if(!(meanOfMin - eps < values[i] && values[i] < meanOfMin + eps)) {
                //oscillation not stable enough (too much decay)
                return 1;
            }
        }


        //everything seems to be fine then
        return 2;
    }

}

<!-- (c) https://github.com/MontiCore/monticore -->
# Application
![pipeline](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/application/badges/master/build.svg)
![coverage](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/application/badges/master/coverage.svg)

The `main()` function of the file `SimulatorMain.java` in the `application` repository can be started from the IDE. This creates a minimal setup for the simulation.

Console and plotter results are created as outputs and it can be used for debugging.

This class specifies its own simulation scenarios independently from the simulation start with visualization support.

For reference, this repository also includes the old code that was used for the controller tuning.

# Driving Scenarios
Different driving scenarios can be loaded in the file `SimulatorMain.java`.

The corresponding code blocks can be enabled / disabled in the `runSimulation()` function of the `SimulatorMain.java` class.
